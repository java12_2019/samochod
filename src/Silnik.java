public class Silnik {
    private int moc;
    private int pojemnosc;

    public Silnik(int moc, int pojemnosc) {
        this.moc = moc;
        this.pojemnosc = pojemnosc;
    }

    public int getMoc() {
        return moc;
    }

    public void setMoc(int moc) {
        this.moc = moc;
    }

    public int getPojemnosc() {
        return pojemnosc;
    }

    public void setPojemnosc(int pojemnosc) {
        this.pojemnosc = pojemnosc;
    }

    public static void run(){
        System.out.println("Brum brum!");
    }

    @Override
    public String toString() {
        return "Silnik{" +
                "moc=" + moc +
                ", pojemnosc=" + pojemnosc +
                '}';
    }
}
