class Podwozie{
    private double dlugosc;
    private String rodzajZawieszenia;

    public double getDlugosc()
    {
        return dlugosc;
    }

    public void setDlugosc(double dlugosc)
    {
        this.dlugosc = dlugosc;
    }

    void wyswietlLiczbeKol(int liczbaKol){
        System.out.println(liczbaKol);
    }
    void testujPodwozie(String rodzajTestu){
        System.out.println("Testuje wg schematu: " + rodzajTestu);
    }

    public String getRodzajZawieszenia()
    {
        return rodzajZawieszenia;
    }

    public void setRodzajZawieszenia(String rodzajZawieszenia)
    {
        this.rodzajZawieszenia = rodzajZawieszenia;
    }

}